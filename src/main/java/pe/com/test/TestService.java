package pe.com.test;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

@Path("Service")
public class TestService {
	
	@GET
	@Produces("application/json")
	@Path("conversor")
	public Response convertFtoC() throws JSONException {
 		//esta es una prueba para verificar la actualizacion del jenkis
		JSONObject jsonObject = new JSONObject();
		Double fahrenheit = 98.24;
		Double celsius;
		celsius = (fahrenheit - 32) * 5 / 9;
		jsonObject.put("FValue", fahrenheit);
		jsonObject.put("CValue", celsius);
 
		String result = "@Produces(\"application/json\") Output: \n\nF to C Converter Output: \n\n" + jsonObject;
		return Response.status(200).entity(result).build();
		//return Response.status(200).entity(jsonObject).build();
	}

}
